using System;
using Xunit;
using @new.Controllers;
using System.Linq;
using System.Drawing;
using System.Threading;


using Microsoft.VisualStudio.TestPlatform.ObjectModel;
using Microsoft.VisualStudio.TestPlatform.ObjectModel.Client;
using Microsoft.VisualStudio.TestPlatform.ObjectModel.Client.Interfaces;
using Microsoft.VisualStudio.TestPlatform.ObjectModel.Logging;
namespace XUnitTestProjectWebApi
{
    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {
            var test1 = new ValuesController();
            var result = test1.Get();
            Assert.Equal(2, result.Count());
        }
    }
}
